const router = require('express').Router();

const authCheck = (req, res, next) => {
    if(!req.user){
      //if user not logged in
        res.redirect('/auth/login');
    } else {
      //if logged inmove to next middleware
        next();
    }
};

router.get('/', authCheck, (req, res) => {

  res.render('profile', { user: req.user });
});

module.exports = router;
